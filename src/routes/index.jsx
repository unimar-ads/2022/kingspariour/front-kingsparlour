import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'

import {Login} from '../pages/signin'
import { Register } from '../pages/signout'
import { Dashboard } from '../pages/dashboard'
import { Profile } from '../pages/profile'
import { Table } from '../pages/appointment table'
 
export const AppRouter = () => {
    return(
        <Router>
            <Routes>
                <Route path='/' exact element={<Login />} />
                <Route path='/login' exact element={<Login />} />
                <Route path='/register' exact element={<Register />} />
                <Route path='/dashboard' exact element={<Dashboard />} />
                <Route path='/profile' exact element={<Profile />} />
                <Route path='/table' exact element={<Table />} />
            </Routes>
        </Router>
    )
}