import React from 'react';
import { Link } from 'react-router-dom';

import { Container, Content, Profile } from './styles';

export const Header = () => {
  return(
    <Container>
      <Content>
        <nav>
          <Link to="/dashboard">King's Parlour</Link>
        </nav>
        <aside>
          <Profile>
            <div>
              <Link to="/profile">Meu Perfil</Link>
            </div>
          </Profile>
        </aside>
      </Content>
    </Container>
    )
}