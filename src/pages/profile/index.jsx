import React from "react";
import { Link } from "react-router-dom";
import { Form, Input } from '@rocketseat/unform';

import { Container, Exterior } from './styles';
import {Header} from '../../components/header';

export const Profile = () => {
  return (
    <Exterior>
      <Container>
        <Header></Header><Form>
          <Input name="name" placeholder="Nome completo" />
          <Input name="email" placeholder="Seu endereço de email" />
          <Input name="cpf" placeholder="Seu CPF" />
          <Input name="phone" placeholder="Seu celular" />

          <hr />

          <Input
            type="password"
            name="oldPassword"
            placeholder="Sua senha atual"
          />
          <Input type="password" name="password" placeholder="Nova Senha" />
          <Input
            type="password"
            name="confirmPassword"
            placeholder="Confirmação de senha"
          />

          <button type="submit">Atualizar perfil</button>
        </Form>
          
        <button type="button">
          <Link to="/login">Sair de KingsParlour</Link>
        </button>
      </Container>
    </Exterior>
  );
}