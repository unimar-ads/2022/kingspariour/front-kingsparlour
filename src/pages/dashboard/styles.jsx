import styled from "styled-components";

export const Container = styled.div`
width: 100%;
min-height: 100vh;
background-color: #111;
`
export const Content = styled.div`
max-width: 600px;
margin: 50px auto;
display: flex;
flex-direction: column;
header {
  display: flex;
  align-self: center;
  align-items: center;
  button {
    border: 0;
    background: none;
  }
  strong {
    color: #fff;
    font-size: 24px;
    margin: 0 15px;
  }
}
ul {
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 15px;
  margin-top: 30px;
}
`
export const Button = styled.button`
  background-color: #D9D9D9;
  color: black;
  padding: 20px;
  border-radius: 40px;
  outline: 0;
  box-shadow: 0px 2px 2px gray;
  cursor: pointer;
  transition: ease background-color 250ms;
  font-size: 25px;
  line-height: 1.2;
  margin: 10px 0px;
  &:hover {
    background-color: #A0A0A0;
  }
`